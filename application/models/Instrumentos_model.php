<?php
class Instrumentos_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_instrumentos () {
        $query = $this->db->get('instrumentos');
        return $query->result_array();
    }

}