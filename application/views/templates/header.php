<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>TuneBook - <?php echo $title; ?></title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/css/materialize.min.css') ?>"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>

    <nav>
        <div class="nav-wrapper deep-purple darken-3">
            <div class="container">
                <a href="<?php echo site_url() ?>" class="brand-logo">TuneBook</a>

                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="#">Grupos</a></li>
                    <li><a href="<?php echo site_url('instrumentos') ?>">Instrumentos</a></li>
                    <?php
                        if(isset($this->session->userdata['logged_in'])){ ?>
                            <li><a href="#">Perfil</a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo site_url('login') ?>">Login</a></li>
                        <?php } ?>
                </ul>
            </div>
            <a href="#" data-activates="side-main" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul id="side-main" class="side-nav">
                <li><a href="#">Grupos side</a></li>
                <li><a href="<?php echo site_url('instrumentos') ?>">Instrumentos</a></li>
                <?php
                if(isset($this->session->userdata['logged_in'])){ ?>
                    <li><a href="#">Perfil</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo site_url('login') ?>">Login</a></li>
                <?php } ?>
            </ul>
        </div>
    </nav>

    <div id="main-container" class="container">