<div id="instrumentos" class="row">

    <div class="collection">

        <?php foreach ($instrumentos as $instrumento_item): ?>

        <a href="<?php echo site_url('instrumentos/'.$instrumento_item['nombre']); ?>" class="collection-item">
            <?php echo $instrumento_item['nombre']; ?>
        </a>

        <?php endforeach; ?>

    </div>
</div>
