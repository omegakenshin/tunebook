<div id="news" class="row">

<?php foreach ($news as $news_item): ?>

    <div class="news-item col s12 m6">

        <div class="card purple deep-purple lighten-5">
            <div class="card-content">
                <span class="card-title">
                    <h4>
                        <a href="<?php echo site_url('news/'.$news_item['slug']); ?>" class="deep-purple-text">
                            <?php echo $news_item['title']; ?>
                        </a>
                    </h4>
                </span>
                <p class="flow-text"><?php echo $news_item['text']; ?></p>
            </div>
            <div class="card-action right-align">
                <a href="<?php echo site_url('news/'.$news_item['slug']); ?>"
                   class=" waves-effect waves-light btn deep-purple darken-1">
                    <i class="material-icons left">label</i>
                    View article
                </a>
            </div>
        </div>

    </div>

<?php endforeach; ?>
</div>
