<?php
if (isset($this->session->userdata['logged_in'])) {
    header("location: http://localhost/tuneBook/index.php/user_authentication/user_login_process");
}
?>

<div id="main">
    <div id="login">
        <h4>Formulario de registro</h4>
        <?php
        if(validation_errors() != false) {
            echo "<div class='error_msg card-panel red darken-1 white-text'>";
            echo validation_errors();
            echo "</div>";
        }
        echo form_open('user_authentication/new_user_registration');
        echo"<div class=\"input-field col s12 m6\">";
        echo form_label('Crear usuario: ');
        echo form_input('username');
        if (isset($message_display)) {
            echo "<div class='error_msg card-panel red darken-1 white-text'>";
            echo $message_display;
            echo "</div>";
        }
        echo"</div>";
        echo"<div class=\"input-field col s12 m6\">";
        echo form_label('Email : ');

        $data = array(
            'type' => 'email',
            'name' => 'email_value'
        );
        echo form_input($data);
        echo"</div>";
        echo"<br/>";
        echo"<div class=\"input-field col s12 m6\">";
        echo form_label('Password : ');
        echo form_password('password');
        echo"</div>";
        echo"<div class='center-align'>";
        echo form_submit('submit', 'Sign Up', "class='waves-effect waves-light btn deep-purple darken-1'");
        echo"</div>";
        echo form_close();
        ?>
        <br>
        <div class='center-align'>
            <a href="<?php echo site_url('login') ?> " class="waves-effect waves-light btn deep-purple darken-1">
                <i class="material-icons left">assignment_ind</i>
                Ir a login
            </a>
        </div>
    </div>
</div>
