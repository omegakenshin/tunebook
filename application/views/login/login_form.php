<?php
if (isset($this->session->userdata['logged_in'])) {

    header("location: http://localhost/tuneBook/index.php/user_authentication/user_login_process");
}
?>

<?php
if (isset($logout_message)) {
    echo "<div class='message'>";
    echo $logout_message;
    echo "</div>";
}
?>
<?php
if (isset($message_display)) {
    echo "<div class='message card-panel blue darken-1 white-text'>";
    echo $message_display;
    echo "</div>";
}
?>
<div id="main">
    <div id="login" class="row">
        <h4 class="center-align">Login Form</h4>
        <?php echo form_open('user_authentication/user_login_process'); ?>
        <?php
        if (isset($error_message)) {
            echo "<div class='error_msg card-panel red darken-1 white-text'>";
            echo $error_message;
            echo validation_errors();
            echo "</div>";
        }
        ?>
        <div class="input-field col s12 m6">
            <i class="material-icons prefix">account_circle</i>
            <label>UserName :</label>
            <input type="text" name="username" id="name" placeholder="username"/>
        </div>
        <div class="input-field col s12 m6">
            <i class="material-icons prefix">lock</i>
            <label>Password :</label>
            <input type="password" name="password" id="password" placeholder="**********"/>
        </div>
        <div class="col s12 center-align">
            <input type="submit" value=" Login " name="submit" class="waves-effect waves-light btn deep-purple darken-1"/>
        </div>
        <div class="col s12 center-align">
            <br><br>
            <a href="<?php echo site_url('user_authentication/user_registration_show') ?>" class="waves-effect waves-light btn deep-purple darken-1">
                <i class="material-icons left">assignment_ind</i>
                Registrarse
            </a>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
