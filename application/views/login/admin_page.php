<?php
if (isset($this->session->userdata['logged_in'])) {
    $username = ($this->session->userdata['logged_in']['username']);
    $email = ($this->session->userdata['logged_in']['email']);
} else {
    header("location: login");
}
?>

<div id="profile" class="container">
    <?php
    echo "<br/>";
    echo "Hello <b id='welcome'><i>" . $username . "</i> !</b>";
    echo "<br/>";
    echo "<br/>";
    echo "Welcome to Admin Page";
    echo "<br/>";
    echo "<br/>";
    echo "Your Username is " . $username;
    echo "<br/>";
    echo "Your Email is " . $email;
    echo "<br/>";
    echo "<br/>";
    ?>
    <b id="logout"><a href="<?php echo site_url('user_authentication/logout') ?>" class="waves-effect waves-light btn deep-purple darken-1">Logout</a></b>
</div>
