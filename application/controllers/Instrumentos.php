<?php
class Instrumentos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('instrumentos_model');
        $this->load->helper('url_helper');
    }

    public function index() {

        $data['instrumentos'] = $this->instrumentos_model->get_instrumentos();
        $data['title'] = 'Instrumentos';

        $this->load->view('templates/header', $data);
        $this->load->view('instrumentos/index', $data);
        $this->load->view('templates/footer');

    }


}